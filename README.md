# install and activate my base software

```
sudo apt install vim graphviz doxygen zsh ninja-build ack-grep
chsh -s /usr/bin/zsh
```

download and install visual studio code
  - install c++ and cmake extensions

start vim and run :PlugInstall


# Link config files

```
cd
ln -s dotfiles/.vim .
ln -s .vim/init.vim .vimrc
ln -s dotfiles/.zsh .
ln -s dotfiles/.zshrc .
```

# apply retina settings

- install pupi theme https://www.xfce-look.org/p/1016275/
- change scale factor for gtk programms `gsettings set org.gnome.desktop.interface scaling-factor 2`
- xfce appearance fonts dpi 150

# keyboard settings

set iso layout
```
apt install sysfsutils
```
add following lines to /etc/sysfs.conf:
```
# apple keyboard swap <> / °^
module/hid_apple/parameters/iso_layout = 0
```
