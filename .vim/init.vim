set nocompatible
set wildmenu
set hidden
set hlsearch
set incsearch
set exrc
set secure
set splitbelow
set splitright
set colorcolumn=110
set browsedir=buffer
set laststatus=2
set clipboard=unnamed
set ts=4
set sw=4
set mouse=a
set encoding=utf-8
set autowrite
let mapleader=","
set backspace=eol,indent,start

let mapleader=","

" support json beautify via nodejs
let @j='IJSON.stringify(A,null,4):.!node -p'
let @j=':%!python -mjson.tool'

syntax enable
highlight ColorColumn ctermbg=darkgray

filetype off

call plug#begin('~/.vim/plugged')

"Improve searchability
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'mileszs/ack.vim'
Plug 'tpope/vim-unimpaired'

"Colorschemes
Plug 'nanotech/jellybeans.vim'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'yssl/QFEnter'

Plug 'bitc/vim-bad-whitespace'

" Plug 'Valloric/YouCompleteMe', { 'do': './install.py --clang-completer' }
" First install vim-lsp, this plugin relies on it:
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'pdavydov108/vim-lsp-cquery'
Plug 'prabirshrestha/asyncomplete-lsp.vim'

Plug 'vim-scripts/localrc.vim'

Plug 'SirVer/ultisnips'

call plug#end()

" Configure Ack
let g:ackprg = 'ag --vimgrep'

colorscheme jellybeans

filetype plugin indent on

let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"
let g:UltiSnipsSnippetDirectories=[$HOME.'/.vim/UltiSnips']

set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " Linux/MacOSX

" Setup netrw browsing
let g:netrw_banner=0
let g:netrw_browse_split=4
let g:netrw_altv=1
let g:netrw_liststyle=3
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',~(^\|\s\s\)\zs\.\S\+'

if has('gui_running')
	set guifont=Mensch:h13
	set guifont=hasklig:h14
	set guifont=GoMono:h14
endif

autocmd BufNewFile,BufFilePre,BufRead *.md set filetype=markdown
autocmd BufNewFile,BufRead *.ts,*.tsx setlocal filetype=typescript

au BufRead,BufNewFile *.py,*.pyw,*.c,*.h,*.cpp,*.hpp match BadWhitespace /\s\+$/


" Indent options recognize c++ namespace indent.
set cindent
set cinoptions+=N-s,g-s,j1,(0,ws,Ws


" c++ code navigation / lsp support
if executable('cquery')
   au User lsp_setup call lsp#register_server({
      \ 'name': 'cquery',
      \ 'cmd': {server_info->['cquery']},
      \ 'root_uri': {server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'compile_commands.json'))},
      \ 'initialization_options': { 'cacheDirectory': '/tmp/jens-cquery-cache' },
      \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp', 'cc', ],
      \ })
endif

" Mappings

vmap r "_dP
nnoremap <leader>gt :LspDefinition<CR>
nnoremap <leader>gs :LspTypeDefinition<CR>
nnoremap <leader>gi :LspImplementation<CR>
nnoremap <leader>fr :LspReferences<CR>
nnoremap <leader>fs :LspWorkspaceSymbol<CR>
nnoremap <leader>fd :LspCqueryDerived<CR>
nnoremap <leader>fc :LspCqueryCallers<CR>


nmap <Leader>t :call ToggleFileByExtension()<cr>
nmap <Leader>o :FZF<cr>
nnoremap <Leader>a :Ack 
nnoremap <Leader>s :Ack <cword><cr>

nnoremap <leader>cc :cclose<CR>
nnoremap <leader>cn :cnext<CR>
nnoremap <leader>cp :cprev<CR>


"Functions
"
"
function! Myfzf(filename)
	exe ":FZF"
	call feedkeys(a:filename)
endfunction

function! GotoFile(filename)
    exe ":e " a:filename
    "exe ":tab drop " a:filename
    "exe ":tab sbuffer " a:filename
endfunction

function! TryToggleByExtension(dict, file, ext)
    if has_key(a:dict, a:ext)
        let l:s = get(a:dict, a:ext, a:ext)
        let l:nf = a:file . "." . l:s
        if filereadable(l:nf)
            call GotoFile(l:nf)
            return 1
        endif
    endif
endfunction

function! ToggleFileByExtension()
    let l:file = expand('%:r') 
    let l:ext = expand('%:e')
    let l:a = [
\       { 'cpp' : 'h', 'h' : 'cpp' , 'vs' : 'fs' , 'fs' : 'vs' },
\       { 'm' : 'h', 'h' : 'm' },
\       { 'mm' : 'h', 'h' : 'mm' },
\       { 'cc' : 'h', 'h' : 'cc' },
\       { 'cpp' : 'hpp', 'hpp' : 'cpp' },
\       { 'cxx' : 'hxx', 'hxx' : 'cxx' }
\   ]
    for l:d in l:a
        if TryToggleByExtension(l:d, l:file, l:ext)
            return
        endif
    endfor 
endfunction


set secure
