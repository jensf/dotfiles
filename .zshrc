setopt nobeep
setopt correct
setopt extendedglob
setopt SHARE_HISTORY

bindkey -e
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.history
EDITOR=vim

autoload -Uz compinit
compinit -u
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache


export PS1='%m:%1~|%# '

export LANG=en_US.UTF-8

__git_files () { 
    _wanted files expl 'local files' _files 
}

GOPATH=$HOME/Go
export GOPATH
path=( \
	/usr/lib/ccache \
	~/chromium.googlesource.com/chromium/tools/depot_tools \
	~/bin \
	~/Tools/bin \
	$GOPATH/bin \
	~/Html5/bin \
	~/clion-2017.1.1/bin \
	/usr/local/bin \
	$path \
	)

alias ls='ls -G --color=auto'
alias ll='ls -laG --color=auto'

export EDITOR='vim'

fpath=(~/.zsh/functions $fpath)

for f in $(ls ~/.zsh/functions/*); do
	autoload $(basename $f)
done


# Enable Ctrl-x-e to edit command line
autoload -U edit-command-line
# Emacs style
zle -N edit-command-line
bindkey '^xe' edit-command-line
bindkey '^x^e' edit-command-line



[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
